var express = require('express'), io = require('socket.io');;

var app = express()
  , server = require('http').createServer(app)
  , io = io.listen(server);

app.use('/', express.static(__dirname + '/../src'));

server.listen(8080);

var interval = setInterval(function() { 
    var val = 1 + (Math.random() - 0.5);
    io.sockets.emit('update', { code: 'usd/gbp', value: (0.9 +  Math.random() / 5), time: new Date().toLocaleTimeString() }); 
}, 1000);

console.log('Listening on port 8080');