define(
	'dataprovider',
	function(require) {
		DataProvider = function() {
			this._started = true;
		};

		DataProvider.prototype.start = function() {
			this._started = true;
		};

		DataProvider.prototype.stop = function() {
			this._started = false;
		};

		DataProvider.prototype.ondata = null;

		return DataProvider;
	}
)