define(
    'chart',
    function(require) {    	
    	var DataAnimation = require('dataanimation');

		var Chart = function (elem) {
			var self = this;

			this._canvasElement = elem;
			this._context = elem[0].getContext('2d');
			this._chartAnimation = null;
			this._padding = 30;
			this._stepSize = 10; /* px */
			this._data = new DataAnimation(10 /* steps */, this._stepSize, this._padding, this._canvasElement.width(), this._canvasElement.height());
			this._data.requestForRedraw = function() { self.redrawFromStart(); };
			this._scale = 1;
			this._maxVal = -Infinity;
			this._minVal = Infinity;
			this._timestamps = [];
			this._xlabelStep = 3; // drow a label on X axis every 3 data values (timestamps)
			this._zoom = 0.8;
		};

		Chart.prototype.redrawFromStart = function() {
			this.redraw();
			this._timestamps = [];
		};

		Chart.prototype.stopAnimation = function(){
			if (this._chartAnimation && window.cancelAnimationFrame) {
				window.cancelAnimationFrame(this._chartAnimation);
			};
		};

		Chart.prototype.clearCanvas = function () {
			if (this._context && this._canvasElement)
			{
				this._context.save();
				this._context.setTransform(1, 0, 0, 1, 0, 0);
				this._context.clearRect(0, 0, this._canvasElement.width(), this._canvasElement.height());
				this._context.restore();
			}
		};

		Chart.prototype._updateScale = function(val) {
			if (val != 0) { 				
				if ((val > this._maxVal) || (val < this._minVal)) {	// got a bigger value, should rescale
					if (val > this._maxVal) this._maxVal = val;
					if (val < this._minVal) this._minVal = val;
					var size = (this._maxVal + this._minVal) / 2;
					this._scale = this._canvasElement.height() / (this._maxVal - this._minVal);
					this._scale = this._scale * this._zoom; // let's keep some margins for a graph inside canvas
					this._data.setTransform(this._scale, size);
					this.redraw();
				}
			}
		};

		Chart.prototype.addValue = function(val) {
			this._updateScale(val.value);
			this._data.addValue(val.value);
			this._timestamps.push(val.time);
			if (this._timestamps.length % this._xlabelStep == 0) {
				this.redraw();
			}
		};

		Chart.prototype.drawAxes = function() {
			var context = this._context;
			context.beginPath();
			context.strokeStyle = '#ccc';
			context.moveTo(this._padding, 0);
			context.lineTo(this._padding, this._canvasElement.height() - this._padding);
			context.lineTo(this._canvasElement.width(), this._canvasElement.height() - this._padding);
			context.stroke();

			context.beginPath();
			context.strokeStyle = '#F33';
			context.moveTo(this._padding, this._canvasElement.height() / 2);
			context.lineTo(this._canvasElement.width(), this._canvasElement.height() / 2);			
			context.stroke();

			// draw labels
			context.textAlign = "right"
			context.textBaseline = "middle"; 
			context.font = 'italic 8px Calibri';
			offset = (this._maxVal + this._minVal) / 2;

			var Y_LABELS = 10;
			// y axis
			for(var i = this._minVal; i < this._maxVal; i += (this._maxVal - this._minVal) / Y_LABELS) {
			    context.fillText(i.toFixed(2), this._padding, - (i - offset) * this._scale + this._canvasElement.height() / 2);
			}

			// x axis
			for (var i = 0; i < this._timestamps.length; i+= this._xlabelStep) {
				context.fillText(this._timestamps[i], this._padding * 1.5 + i * this._stepSize, this._canvasElement.height() - this._padding / 2 + (i % 2 * 10));
			};
		};

		Chart.prototype.drawFrame = function() {
			var context = this._context;
			this._data.drawFrame(context);
		};

		Chart.prototype.redraw = function() {
			this.clearCanvas();			
			this.drawAxes();
			this._data.redraw(this._context);
		};

		Chart.prototype.init = function () {
			var self = this;			
			(function animloop(time) {
		      	self.drawFrame();				
		      	self._chartAnimation = requestAnimFrame(animloop, self._canvasElement);
		    })();
		};

        return Chart;
    }
);