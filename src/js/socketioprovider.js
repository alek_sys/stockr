define(
	'socketioprovider',
	function(require) {
		var DataProvider = require('dataprovider');

		var SocketIOProvider = function(url) {
			var self = this;
			SocketIOProvider.superclass.constructor.apply(this)
			var socket = io.connect(url);
		    socket.on('update', function (data) {
		    	if (self.ondata && self._started) {
		    		self.ondata(data);
		    	}
		    });

		    socket.on('error', function () { console.log('error'); }) ;
		    socket.on('connect_failed', function () { console.log('conenct failed'); }); 
		};

		extend(SocketIOProvider, DataProvider);

		return SocketIOProvider;
	}
)