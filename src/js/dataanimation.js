define(
    'dataanimation',
    function(require) {
        var DataAnimation = function(steps, xscale, paddingx, width, height) {
    		this._stepY = 0; 
    		this._stepX = 0;
    		this._prevValue = null;
    		this._xscale = xscale;
    		this._steps = steps;
            this._startX = paddingx;
    		this._x = this._startX;
    		this._y = 0;
    		this._stepsLeft = 0;
            this._data = [ ];   // stores initial data - required for a redraw
            this._scale = 1;
            this._offset = 0;
            this._width = width;
            this._height = height;
            this._valueIndex = 1;
    	};

        DataAnimation.prototype.setTransform = function(scale, offset) {
            this._scale = scale;
            this._offset = offset;
            this._stepsLeft = 0;            
        };

        DataAnimation.prototype._translateValue = function(value) {            
            return -(value - this._offset) * this._scale + this._height / 2;
        };

    	DataAnimation.prototype.addValue = function (value) {
            this._data.push(value);            
    		if (!this._prevValue) {
    			this._prevValue = value;
    		}
    		else {
                this._x = (this._data.length - 2) * this._xscale + this._startX;
                this._y = this._prevValue;
    			this._stepY = (value - this._prevValue) / this._steps;
    			this._stepX = this._xscale / this._steps;
    			this._stepsLeft = this._steps;
                this._prevValue = value;                
    		}
    	};

    	DataAnimation.prototype.drawFrame = function(context) {
    		if (this._stepsLeft <= 0) {
                if (this._x >= this._width) {
                    this._x = this._startX;
                    this._data = [this._data[this._data.length - 1]];
                    this._prevValue = this._data[0];
                    if (this.requestForRedraw) {
                        this.requestForRedraw();
                    }
                }
            } else {
        		var nextX = this._x + this._stepX;
        		var nextY = this._y + this._stepY;

        		context.save();
        		this._line(context, this._x, this._translateValue(this._y), nextX, this._translateValue(nextY));
        		context.restore();

        		this._x = nextX;
        		this._y = nextY;
        		this._stepsLeft -= 1;
            }
    	};

        DataAnimation.prototype._line = function(context,x1,y1,x2,y2) {
            context.strokeStyle = '#00F';
            context.beginPath();
            context.moveTo(x1, y1);
            context.lineTo(x2, y2);
            context.stroke();
        };

        DataAnimation.prototype.redraw = function(context) {
            var x = this._startX, y = this._translateValue(this._data[this._valueIndex - 1]);
            for (var i = this._valueIndex; i < this._data.length; i++) {
                var value = this._translateValue(this._data[i]);
                this._line(context, x, y, x + this._xscale, value);

                x = x + this._xscale; y = value;
            };

            var offsetX = (this._x % this._xscale);
            this._x = (x - this._xscale) + offsetX;
        };

        return DataAnimation;
    }
);