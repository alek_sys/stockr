define(
    'main',
    function(require) {
		var chartElem = $('#chart');

    	var Chart = require('chart');
    	var SocketIOProvider = require('socketioprovider');    	
    	
    	var chart = new Chart(chartElem);
    	var provider = new SocketIOProvider('http://localhost:8080');
    	provider.ondata = function(data) { chart.addValue(data); };
    	
    	chart.init();

        $('body').focus(function(){ chart.redraw(); });
		$('#start').click(function(){provider.start();});
    	$('#stop').click(function(){provider.stop();});
    	$('#redraw').click(function(){chart.redraw();});
    }
);
