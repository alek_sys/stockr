function extend(Child, Parent) { 
	var F = function() { }; 
	F.prototype = Parent.prototype; 
	Child.prototype = new F(); 
	Child.prototype.constructor = Child; 
	Child.superclass = Parent.prototype;
}; 

window.requestAnimFrame = (function(){
		      return  window.requestAnimationFrame       || 
		              window.webkitRequestAnimationFrame || 
		              window.mozRequestAnimationFrame    || 
		              window.oRequestAnimationFrame      || 
		              window.msRequestAnimationFrame     || 
		              function(/* function */ callback, /* DOMElement */ element){
		                window.setTimeout(callback, 1000 / 60);
		              };
		    })();